FROM erlang:19.2.3
MAINTAINER Heinz N. Gies <heinz@project-fifo.net>

# Install Dalmatiner-Graphite
RUN apt-get update && apt-get install -y python-setuptools
RUN git clone https://github.com/pakosh/dalmatiner-graphite.git /tmp/dalmatiner-graphite
RUN cd /tmp/dalmatiner-graphite && \
    python setup.py install && \
    cd / && \
    rm -rf /tmp/dalmatiner-graphite
RUN apt-get autoremove && apt-get clean
EXPOSE 2003

COPY ddbgraphite.sh /

VOLUME ["/data"]

ENTRYPOINT ["/ddbgraphite.sh"]
